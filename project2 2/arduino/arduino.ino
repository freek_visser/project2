#include <Wire.h>

#define rechts1 5
#define rechts2 4
#define links1 6
#define links2 7
#define irLeft 8
#define irRight 3
#define irFront 12
#define reed 13

#define trig 10
#define echo 11

int duration;
int distance;
bool automatic;

void ForWard()                      //method for driving forward
 {
  digitalWrite(rechts1, HIGH);
     digitalWrite(rechts2, LOW);
       digitalWrite(links1, HIGH);
     digitalWrite(links2, LOW);
 }
  void sTOP()                       //method for stopping the robot
 {
  digitalWrite(rechts1, LOW);
     digitalWrite(rechts2, LOW);
       digitalWrite(links1, LOW);
     digitalWrite(links2, LOW);
 }
 
 void links()                      //method for driving to the left
 {
    digitalWrite(rechts1, LOW);
  digitalWrite(rechts2, HIGH);
  digitalWrite(links1, HIGH);
  digitalWrite(links2, LOW);
 }
 void rechts()                    //method for driving to the right
 {
  digitalWrite(rechts1, HIGH);
  digitalWrite(rechts2, LOW);
  digitalWrite(links1, LOW);
  digitalWrite(links2, HIGH);
 }
 void sensor()                    
 {
     digitalWrite(rechts1, LOW);
     digitalWrite(rechts2, HIGH);
       digitalWrite(links1, LOW);
     digitalWrite(links2, HIGH);
 }
 void achteruit(){              //method for reversing
    digitalWrite(rechts1, LOW);
     digitalWrite(rechts2, HIGH);
       digitalWrite(links1, LOW);
     digitalWrite(links2, HIGH);
 }
void setup() {
  // put your setup code here, to run once:
  //motors
  pinMode(rechts1, OUTPUT);
  pinMode(rechts2, OUTPUT);
   pinMode(links1, OUTPUT);
  pinMode(links2, OUTPUT);
  //ir's
   pinMode(irLeft, INPUT);
  pinMode(irRight, INPUT);
  pinMode(irFront, INPUT);
  //ultrasone sensor
  pinMode(echo, INPUT);
  pinMode(trig, OUTPUT);
  //reed
    pinMode(reed, INPUT);
  Serial.begin(9600); 
  sTOP();
  Wire.beginTransmission(1);
  Wire.onReceive(receiveEvent);
}

void loop() {
  if(automatic){                        //if the controller has instructed an automatic drive for the robot, execute following code
  int irLeftA = digitalRead(irLeft);    //save IR values as integers
  int irFrontA = digitalRead(irFront);
  int irRightA = digitalRead(irRight);                                      
  
 //calculating distance ultrasone sensor
 digitalWrite(trig , HIGH);
  delayMicroseconds(1000);
  digitalWrite(trig , LOW);
  
  duration = pulseIn(echo , HIGH);
  distance = (duration/2) / 28.5 ;
  Serial.println(distance);
 
 if(irLeftA == LOW && irFrontA == LOW && irRightA == LOW){    //if no obstacles are detected, keep driving straight

ForWard();
 }
 if(irLeftA == HIGH && irFrontA == LOW && irRightA == LOW){   //if left IR is detecting an obstacle, drive back and avoid the obstacle
  achteruit();
  delay(350);
  links();
  delay(425);
  ForWard();
  delay(200);
 }
 if(irLeftA == LOW && irFrontA == HIGH && irRightA == LOW){   //if front IR is detecting an obstacle, drive back and avoid the obstacle
   achteruit();
  delay(450);
  rechts();
  delay(500);
  ForWard();
  delay(200);
 }
  if(irLeftA == LOW && irFrontA == LOW && irRightA == HIGH){  //if right IR is detecting an obstacle, drive back and avoid the obstacle
    achteruit();
    delay(400);
     rechts();
     delay(500);
    ForWard();
     delay(200);
  }
  if(distance < 4){                                          // if the ultrasone sensor is detecting an obstacle, drive back and avoid the obstacle
     achteruit();
    delay(250);
     rechts();
     delay(150);
    ForWard();
     delay(200);
  }
  if(reed == HIGH){
    sTOP();
}
}
}

void receiveEvent(){
  int index = Wire.read();            //save the i2c data as integers to send to the method
  int pinValue = Wire.read();
  communication(index, pinValue);
  Wire.endTransmission();
}

void communication(int index, int pinValue){
  if(pinValue == 1){                            //if the button is pressed
      if(index == 0){                           //0 = forward
        ForWard();
      }
      else if(index == 1){                      //1 = left
        links();
      }
      else if(index == 2){                      //2 = right
        rechts();
      }
      else if(index == 3){                      //3 = backwards
        achteruit();
      }
      else if(index == 4){                      //4 = automatic/manual switch
        bool automatic = false;
      }
    }
   else if(pinValue == 0 && index == 4){        //if 4 is not pressed, the car should start driving automatically
    bool automatic = true;
   }
   else{                                        //if pins 0-3 are not pressed, stop the car
      sTOP();
  }
}
